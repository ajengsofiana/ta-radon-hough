clear all;

[filename, pathname] = uigetfile('Pick a file');

% dsm = imread('G:\kuliah\Sem 8\TA\Foto Sapporo\dsm_sapporo.tif');
% intDsm = double(dsm);
dsminputmat = dlmread(strcat(pathname,filename),' ',8,0);
% dsminputshow = imshow(uint8(dsminputmat), [0 255]);
dsmgray = mat2gray(dsminputmat, [0 255]);
% figure; imshow(dsmgray);

% melakukan medianfiltering pada dsm
dsmmedfil = medfilt2(dsmgray);
% figure; imshow(dsmmedfil);

% melakukan contrast enhancement dengan CLAHE
dsmclahe = adapthisteq(dsmmedfil);
% figure; imshow(dsmclahe);

% % melakukan brightening dengan Gamma Transformation
% [lebar,panjang] = size(dsmclahe);
% GammaValue = 0.2;
% dsmgamma = uint8(zeros(lebar,panjang));
% for m = 1 : lebar
%     for n = 1 : panjang
%         dsmgamma(m,n) = 255 * ((dsmclahe(m,n)/255) ^ GammaValue);
%     end
% end
% figure; imshow(dsmgamma);

dsmbright = imadjust(dsmclahe,[],[],0.5);
figure; imshow(dsmbright);

% melakukan binarization dengan thresholding 0,5
dsmbwpre = im2bw(dsmbright,0.5);
% dsmbw = imcomplement(dsmbwpre);
figure; imshow(dsmbwpre);

% melakukan region smoothing menggunakan operasi morfologi

% melakukan closing operation menggunakan disk shape u/ menghaluskan elemen
% dan mengisi lubang2 yg kecil
seclose = strel('disk',4); % cari radius yang optimal
dsmclose = imclose(dsmbwpre,seclose);
figure, imshow(dsmclose);

% melakukan erode operation menggunakan rectangle shape u/ mendeteksi area
% yg luas dan menghapusnya dari gambar
% (ex: menghapus bangunan)
seerode = strel('rectangle',[3 3]); % cari nilai piksel yang paling bagus
dsmerode = imerode(dsmclose,seerode);
figure, imshow(dsmerode);

% dicomplementkan
dsmcom = imcomplement(dsmerode);
figure, imshow(dsmcom);

%thinning
dsmthin = bwmorph(dsmcom,'thin',20);
figure, imshow(dsmthin);

% % cari edge detectionnya pake canny
% dsmedge = edge(dsmcom,'canny');
% figure, imshow(dsmedge);
% 
% cari hough transformnya
[H,T,R] = hough(dsmthin);
imshow(H,[],'XData',T,'YData',R,...
            'InitialMagnification','fit');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;

% cari hough peaksnya
P  = houghpeaks(H,15);
x = T(P(:,2)); y = R(P(:,1));
plot(x,y,'s','color','white');

% cari hough linesnya
lines = houghlines(dsmthin,T,R,P,'FillGap',10,'MinLength',10);
figure, imshow(dsmthin), hold on
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

   % Plot beginnings and ends of lines
   plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
   plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');

   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
end